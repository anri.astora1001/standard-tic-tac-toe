"""prints the current board state"""
def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]

"""tells players who won"""
def game_over(player):
    print_board(board)
    print(player, " has won")
    exit()

"""checks rows takes a string; first, second, or third"""
def is_row_winner(row):
    if(row == "first"):
        return board[0] == board[1] and board[1] == board[2]
    elif(row == "second"):
        return board[3] == board[4] and board[4] == board[5]
    elif(row == "third"):
        return  board[6] == board[7] and board[7] == board[8]

"""checks columns takes a string; first, second, or third"""
def is_column_winner(col):
    if(col == "first"):
        return board[0] == board[3] and board[3] == board[6]
    elif(col == "second"):
        return board[1] == board[4] and board[4] == board[7]
    elif(col == "third"):
        return  board[2] == board[5] and board[5] == board[8]  

"""checks diagonal takes a string; up or down"""
def is_diagonal_winner(direction):
    if(direction == "up"):
        return board[2] == board[4] and board[4] == board[6]
    elif(direction == "down"):
        return board[0] == board[4] and board[4] == board[8]

"""checks for winner and gives winner message"""
def check_winner():
    #checks rows
    if is_row_winner("first"):
        game_over(board[0])
    elif is_row_winner("second"):
        game_over(board[3])
    elif is_row_winner("third"):
        game_over(board[6])
    #checks columns
    elif is_column_winner("first"):
        game_over(board[0])
    elif is_column_winner("second"):   
        game_over(board[1])
    elif is_column_winner("third"):  
        game_over(board[2])
    #checks diagonals 
    elif is_diagonal_winner("down"): 
        game_over(board[0]) 
    elif is_diagonal_winner("up"):
        game_over(board[0])

current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")

    #adds forfeit feature as well as a way to exit the program early
    if(response == "forfeit"):
        print(current_player + " forfeits")
        exit()

    space_number = int(response) - 1
    board[space_number] = current_player

    check_winner()

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")

